<?php if(!defined('KIRBY')) exit ?>

title: Site
pages: true
	template:
		- default
		- imprint
		- home
files: true
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea