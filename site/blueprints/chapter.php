<?php if(!defined('KIRBY')) exit ?>

title: Chapter
pages:
  template: 
  	- chapter
  	- subchapter
files: true
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea
