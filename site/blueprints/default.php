<?php if(!defined('KIRBY')) exit ?>

title: Page
pages:
	template: chapter
files: true
fields:
  title:
    label: Title
    type:  text
  subline:
  	label: Intro to Chapter
  	type: textarea  