O:11:"Cache\Value":3:{s:8:" * value";s:8983:"<!DOCTYPE html>
<html lang="en">
<head>
  <script src="//use.typekit.net/lcf6cua.js"></script>
<script>try{Typekit.load();}catch(e){}</script>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title>Kirby - The Book | Understanding the Kirby Structure</title>
  <script src="http://localhost/kirby-book/assets/js/prism.min.js"></script>
  <link rel="stylesheet" href="http://localhost/kirby-book/assets/css/theme.css"></head>
<body>
<aside>
<nav class="menu" role="navigation">
<h2>Table of Contents</h2>
  
  <ul class="nav-toggle-container cf" id="main-menu">
    
        	<li class="menulink "><a href="http://localhost/kirby-book/about-this-book">About this book</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/what-is-kirby">What is Kirby?</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/getting-started">Getting Started</a></li>
        	<li class="menulink current"><a href="http://localhost/kirby-book/understanding-the-kirby-structure">Understanding the Kirby Structure</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/basic-concepts">Basic Concepts</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/adding-content">Adding Content to Text Files</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/a-short-intro-to-php">A Very Brief Intro to PHP</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/displaying-content">Display your Content</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/working-with-the-panel">The Panel: A backend for Editors</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/multi-language-setup">Multi-Language Setup</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/how-to-create">Solutions</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/adding-a-database">Adding a database</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/backup-and-restoring">Backup and Restoring</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/extending-kirby">Extending Kirby</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/updating-kirby">Updating Kirby</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/helpers">Helpers &amp; Shortcuts</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/configuring-kirby">Configuring Kirby</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/appendix">Appendix</a></li>
        	<li class="menulink "><a href="http://localhost/kirby-book/access-restriction-and-user-roles">Access Restriction and User Roles</a></li>
      </ul>
</nav></aside>
<div class="page-wrapper">
<header>
<button class="menu-toggle closed"><span class="sr-only">Menu</span></button>
</header><nav class="switch" role="navigation">
      <a class="prev" href="http://localhost/kirby-book/getting-started"><span class="sr-only">Previous Page</span></a>
     
    <a class="next" href="http://localhost/kirby-book/basic-concepts"><span class="sr-only">Next Page</span></a>
  </nav>
	<main>
		<h1>Understanding the Kirby Structure</h1>
	<p>Before we dive any deeper, let's take a look at Kirby's structure by exploring  the files and folders in the Kirby starter kit/plainkit, so that we will get a better understanding of what is happening where:</p>
<table>
<thead>
<tr>
<th>File/Folder</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>.htaccess</td>
<td>Required, contains the rewrite rules that make sure that all Kirby components are found</td>
</tr>
<tr>
<td>/assets</td>
<td>Takes your stylesheets, Javascripts, site-wide images etc.</td>
</tr>
<tr>
<td>/content</td>
<td>Contains all content folders and files of the website</td>
</tr>
<tr>
<td>index.php</td>
<td>...</td>
</tr>
<tr>
<td>/kirby</td>
<td>The Kirby core files and the <a href="http://localhost/kirby-book">toolkit</a></td>
</tr>
<tr>
<td>license.md</td>
<td>License File</td>
</tr>
<tr>
<td>/panel</td>
<td>The panel app</td>
</tr>
<tr>
<td>readme.md</td>
<td>As its name suggests</td>
</tr>
<tr>
<td>/site</td>
<td>All structure related files, snippets, templates, config-files etc.</td>
</tr>
<tr>
<td>/thumbs</td>
<td>Takes the thumbs created by the <a href="http://">thumbs() helper</a></td>
</tr>
</tbody>
</table>
<div class="infobox info"><p>The difference between the starterkit and the Plainkit ...</p></div>			<section id="the-assets-folder"> 
			<h2>The Assets Folder</h2>
			<p>The <code>/assets</code> folder contains the assets of your website, like stylesheets, javascript files, fonts, and site-wide images (logos, icons etc.). If you use the panel, it must also contain a writable <code>/avatars</code> folder that takes the user avatars. Basically, the assets folder is the place where all design-related elements of your website reside.</p>
<p>By default, the starterkit/plainkit <code>/assets</code> folder contains the following subfolders:</p>
<ul class="folder"><li>css
</li><li>fonts
</li><li>images
</li><li>js</li></ul>
<p>The `/assets' folder would also be the place where to put any frameworks that you might be using, e.g. if you use a CSS preprocessor like SASS/LESS/Stylus.</p>		</section>
			<section id="the-content-folder"> 
			<h2>The Content Folder</h2>
			<p>Kirby does not use a database. Instead, all content is stored in subfolders of  the <code>/content</code> folder that resides in your Kirby root. Basically, the structure of these folders determines the structure of your site. All additional files like images, videos, PDFs etc. are also stored in those subfolders.</p>
<p>All you need to <a href="http://localhost/kirby-book/add-content">add content</a>, is the Finder on a Mac, Windows Explorer on a Windows system, or their equivalent on Linux-based systems, and a simple <a href="http://localhost/kirby-book/text-editors">text editor</a>. You can, of course, also use a terminal window  to create your folder structure. As an alternative to this &quot;manual&quot; way of doing things, you can install the <a href="http://localhost/kirby-book/panel">Panel</a>, a web interface for interacting with the CMS.</p>
<p>The content folder usually lives within your project root folder but it can also be moved to another location on your host. This can be useful if you want to version control the content folder separately from the rest of your installation, or if you want to share content between different installations of Kirby. More on that <a href="http://localhost/kirby-book">in chapter ...</a>.</p>
<p>At its most basic, the content folder needs to contain at least two folders:</p>
<ul class="folder"><li>home
</li><li>error</li></ul>
<p>(hint: Both of these folders can be renamed if you wish, you just need to tell the system in your (link: text: config.php))</p>
<p>In a &quot;conventionally structured&quot; website, each folder translates into a &quot;page&quot; of the website with the &quot;home&quot; folder at its root, that is the start or &quot;home&quot; page. If you have downloaded the Kirby starterkit, you will see that the content folder contains 5 main folders:</p>
<ul class="folder"><li>1-about
</li><li>2-projects
</li><li>3-contact
</li><li>error
</li><li>home</li></ul>
<p>The &quot;projects&quot;-folder in turn contains some subfolders:</p>
<ul class="folder"><li>1-project-a
</li><li>2-project-b
</li><li>3-project-c</li></ul>		</section>
			<section id="the-site-folder"> 
			<h2>The Site Folder</h2>
			<p>The /site folder, on the other hand, is the place where all your structure-related stuff goes, that is: <a href="http://localhost/kirby-book">templates</a>, <a href="http://localhost/kirby-book">snippets</a>, <a href="http://localhost/kirby-book">custom panel fields</a>, <a href="http://localhost/kirby-book">plugins</a> etc.</p>
<p>The <code>/site</code> folder of the starterkit/plainkit contains the following subfolders:</p>
<ul class="folder"><li>blueprints //only needed if the Panel is used
</li><li>cache
</li><li>config
</li><li>plugins
</li><li>snippets
</li><li>templates</li></ul>
<p>It may also contain a <code>/fields</code> folder that stores your <a href="http://localhost/kirby-book">custom fields</a> for use with the Panel.<br />
If your are using the panel, it must contain an <code>/accounts</code> folder.</p>		</section>
		
	</main>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>    <script>
	    $('.menu-toggle').click(function() {
		if ($(this).hasClass('closed')) {
			$(this).addClass('close').removeClass('closed');
			$('html').addClass('nav-open');
		}
		else if ($(this).hasClass('close')) {
			$(this).removeClass('close').addClass('closed');
			$('html').removeClass('nav-open');
		}
		
		});
    </script>
</body>
</html>";s:10:" * expires";i:1587717917;s:10:" * created";i:1430037917;}