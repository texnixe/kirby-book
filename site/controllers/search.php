<?php

return function($site, $pages, $page) {
  $query = get('q');
  
  $results = $site
                ->search($query, array(
                	'words' => true, 
                	'score' => array(
                		'title' => 1,
                		'text' => 5
                		),
                	'stopwords' => array(
                		'and', 'or', 'but')
                	)
                )
                ->visible()
                ->paginate(10);


  $pagination = $results->pagination();
  $itemCount = $pagination->countItems();
 
  return compact('query','results', 'pagination', 'itemCount');

};