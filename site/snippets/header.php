<!DOCTYPE html>
<html lang="en">
	<head>
	  	<script src="//use.typekit.net/lcf6cua.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>

	  	<meta charset="utf-8" />
	  	<meta name="viewport" content="width=device-width,initial-scale=1.0">

	  	<title><?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?></title>
	  	
	  	<? if($page->description() != ''): ?>
	        <meta name="description" content="<?= $page->description()->html() ?>">
	  	<? else: ?>
	  	    <meta name="description" content="<?= $site->description()->html() ?>">
	  	<? endif ?>
	  	<?php snippet('favicons') ?>
	  	<?php snippet('assets') ?>
	</head>
<body>
	<aside>
		<?php snippet('menu') ?>
	</aside>
<div class="page-wrapper clearfix">
<header>
<button class="menu-toggle closed"><span class="sr-only">Menu</span></button>
<?php if(!$page->isHome()) : ?>   
<h1 class="site-title"><a class="branding" href="<?php echo url() ?>">
        <span class="logo"><img src="<?php echo url() ?>/assets/images/logo.svg" alt="Logo"/></span>
      </a> <?php echo $site->title()->html() ?></h1>

<?php endif ?>
<?php if ($site->user()): ?>
  <!--a href="<?php echo url() . '/panel/#/pages/show/' . $page->uri() ?>">Edit</a-->
<?php endif; ?>
</header>