<?php
//Call uniNextPrev plugin
$uniNextPrev = uninextprev(); 

    $nextPageURL = $uniNextPrev['nextPageURL'];
    $prevPageURL = $uniNextPrev['prevPageURL'];

?>

  <?php if(isset($prevPageURL) AND $prevPageURL<>''): ?>
    <a class="prev" href="<?php echo $prevPageURL ?>"><span class="sr-only">Previous Page</span></a>
  <?php endif ?>
  <?php if(isset($nextPageURL) AND $nextPageURL<>''): ?>
    <a class="next" href="<?php echo $nextPageURL ?>"><span class="sr-only">Next Page</span></a>
  <?php  endif; ?>




