<nav class="switch">
<?php if($page->hasPrev()) : ?>
    <a class="prev" href="<?php echo $page->prev()->url() ?>"><span>back</span></a>
<?php elseif ($page->hasParent()) : echo "true";
	if ($page->parent()->hasPrev() && $page->parent()->prev()->hasChildren()) : ?>
       <a class="prev" href="<?php echo $page->parent()->prev()->children()->last()->url() ?>"><span>back</span></a> 
   <?php endif ?>
<?php endif ?>
<?php if($page->hasNext()): ?> 
    <a class="next" href="<?php echo $page->next()->url() ?>"><span>next</span></a>
<?php elseif ($page->hasParent()) : echo "true";
	if ($page->parent()->hasNext() && $page->parent()->next()->hasChildren()) : ?>
    <a class="next" href="<?php echo $page->parent()->next()->children()->first()->url() ?>"><span>next</span></a>
<?php endif ?>
<?php  endif; ?>
</nav>