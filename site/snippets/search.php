	<span class="label label-primary">Search</span>
	<form class="search" action="<?php echo url('search') ?>" role="search">
	    <div class="searchbox form-group">
	    <label class="search-label sr-only" for="search">Search</label>
	    <input class="searchword form-control"  type="search" id="search" placeholder="Search..." name="q">
	    </div>
	</form>