<?php

	class ChapterPage extends Page {

		public function score() {

				$one = $this->item1()->int();
				$two = $this->item2()->int();
				$three = $this->item3()->int();
				$score = round(($one + $two + $three)/3);
				
				return $score;
		}
	}