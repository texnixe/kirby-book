<?php
kirbytext::$tags['list'] = array(
  'attr' => array(
    'class'
  ),
  'html' => function($tag) {

    $listitems  = array_filter(explode("- ", $tag->attr('list')));
    $class = $tag->attr('class');

    $html = '<ul class="' . $class . '">';
    foreach($listitems as $listitem) {
    	$html .= '<li>' . $listitem . '</li>';
    }
    $html .= "</ul>";

    return $html;

  }
);

