<?php
kirbytext::$tags['quote'] = array(
  'attr' => array(
    'class'
  ),
  'html' => function($tag) {

    $quote  = $tag->attr('quote');
    $class = $tag->attr('class');

    $html = '<q class="' . $class . '">' . $quote . '</q>';

    return $html;

  }
);

