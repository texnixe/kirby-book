<?php

kirbytext::$tags['infobox'] = array(
  'attr' => array(
    'class'
  ),
  'html' => function($tag) {

  	$paragraphs = explode("%%", $tag->attr('infobox'));
    $class = $tag->attr('class');
    $text = ucfirst($class);

    $html = '<div class="infobox ' . $class . '"><div class="badge">' . $text . '</div>';
	foreach($paragraphs as $paragraph) {
    	$html .= '<p>' . $paragraph . '</p>';
    }    
    $html .= "</div>";
    return $html;

  }
);
?>
