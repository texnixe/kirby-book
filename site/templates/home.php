<?php snippet('header') ?>

		<main class="<?php echo $page->uid() ?>">
			<div class="title">
			<h1><?php echo $page->title()->html() ?></h1>
			<h2><?php echo $page->subtitle()->html() ?></h2>
			</div>
		</main>
	</div>
<?php snippet('footer') ?>