<?php snippet('header') ?>
<?php snippet('switch') ?>
<main>
<div class="title">
<?php if($page->num()-1 >0) : ?>
	<h2><?php echo "Chapter" . " " . ($page->num()-1) ;  ?></h2>
<?php endif ?>	
<h1><?php echo $page->title()->html() ?></h1>
<?php echo $page->subline()->kt() ?>
</div>
</main>

<?php snippet('footer') ?>