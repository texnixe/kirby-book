Title: Backup Your Site!

----

Text: 

It goes without saying that keeping regular backups of your site both during development and when the site is live is simply a must.

### Backup the conventional way
Creating a backup of a Kirby website is as easy as it can get. Because Kirby is a file-based CMS without a database, just copy all files to a save place (an external drive, a cloud drive) in regular intervals. You should consider versioning different backups by adding a date and a tag so that you know what changes you made. 

To make a backup of a live site, just download all files and folders via FTP or SSH. If editors can make changes on a live installation, you should run regular backups to make sure that those content changes get backed up.
(infobox: Make sure that you copy all invisible files <a href="kirby-setup">as explained in "Setting up Kirby"</a>. class: info)

### Using Git
A flat-file CMS like Kirby is perfect for use with a versioning control system like (link: text: Git). Using version control and a remote repo, you don't just make sure that you always have a backup of your site. Its particularly great during development, because you can work on different features of your site in separate branches and you can always go back to a stable version if anything ever went wrong. If you want to know more about how you can use Git with Kirby, head over to the (link: working-with-git text: chapter on Git).