Title: Restore Your Site

----

Text: Restoring your site is just as easy as backing it up. **If you have backup**, that is. Upload all the files of your last backup to the server and your site is up running again in no time.