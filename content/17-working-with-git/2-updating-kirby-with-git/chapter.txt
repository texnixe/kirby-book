Title: Updating Kirby with Git

----

Text: 

copied from getkirby.com
### Updating submodules

If you are using Git submodules for the kirby core and the panel in your project, updating via the command line is very easy:

```
git submodule foreach --recursive git checkout master
git submodule foreach --recursive git pull
```

You can also update each submodule individually:

```
cd kirby
git checkout master
git pull
```

### Updating individual repositories

If you've cloned the kirby folder and the panel separately and you don't track them with submodules, use the following lines to update:

```
cd kirby
git checkout master
git pull
git submodule foreach --recursive git checkout master
git submodule foreach --recursive git pull

cd ../panel
git checkout master
git pull
```

(infobox: If you have caching enable, make sure to clean the cache after you have updated Kirby. Otherwise, your site might break. To clean the cache, delete all contents from `/site/cache`. class: warning)