Title: Get the "parent" of a file?

----

Text: 

The "parent" object of a file is actually the page object the file belongs to:

```php
$file->page()
```

returns this page object.