Title: The Site Folder

----

Text: 

The /site folder, on the other hand, is the place where all your structure-related stuff goes, that is: (link: text: templates), (link: text: snippets), (link: text: custom panel fields), (link: text: plugins) etc.

The `/site` folder of the starterkit/plainkit contains the following subfolders:

(list:
- blueprints //only needed if the Panel is used
- cache
- config
- plugins
- snippets
- templates
class: folder)

It may also contain a `/fields` folder that stores your (link: text: custom fields) for use with the Panel.
If your are using the panel, it must contain an `/accounts` folder.