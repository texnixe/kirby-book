Title: The Content Folder

----

Text: 

Kirby does not use a database. Instead, all content is stored in subfolders of  the `/content` folder that resides in your Kirby root. Basically, the structure of these folders determines the structure of your site. All additional files like images, videos, PDFs etc. are also stored in those subfolders.

All you need to (link: add-content text: add content), is the Finder on a Mac, Windows Explorer on a Windows system, or their equivalent on Linux-based systems, and a simple (link: text-editors text: text editor). You can, of course, also use a terminal window  to create your folder structure. As an alternative to this "manual" way of doing things, you can install the (link: panel text: Panel), a web interface for interacting with the CMS.

The content folder usually lives within your project root folder but it can also be moved to another location on your host. This can be useful if you want to version control the content folder separately from the rest of your installation, or if you want to share content between different installations of Kirby. More on that (link: text: in chapter ...).

At its most basic, the content folder needs to contain at least two folders:

(list:
- home
- error
class: folder)

(hint: Both of these folders can be renamed if you wish, you just need to tell the system in your (link: text: config.php))

In a "conventionally structured" website, each folder translates into a "page" of the website with the "home" folder at its root, that is the start or "home" page. If you have downloaded the Kirby starterkit, you will see that the content folder contains 5 main folders:
(list:
- 1-about
- 2-projects
- 3-contact
- error
- home
class: folder)

The "projects"-folder in turn contains some subfolders:

(list:
- 1-project-a
- 2-project-b
- 3-project-c
class: folder)