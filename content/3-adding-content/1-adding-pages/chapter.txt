Title: Adding pages

----

Text: 

Pages are the basic building blocks of your Kirby website. You create a new page by  creating  a new folder inside the `/content` folder and you add content to that page by putting a text file inside each folder. Each folder you add to your Kirby installation is regarded as a separate page that has its own URL.

If, say,  you create a new folder called `/what-we-do` inside of `/content`, you create "yourdomain.com/what-we-do", and if you create a  folder called `/blog`its URL would be "yourdomain.com/blog".

If you create a child folder inside your blog folder, e.g. a blog post folder called `/my-wonderful-first-article`,  this article would be accessible in your browser at "yourdomain.com/blog/my-wonderful-first-article". 

The `home` folder inside the `/content` folder constitutes the base URL of your site and accessible as "yourdomain.com".

(infobox: Folder names must not contain spaces or special characters other then hyphens [-] or underscores [_]. It is also recommended to only use lower case letters for cross system compatibility.  class: warning)

### Different Types of Pages
You may think that having only one sort of content model would be rather limiting.  But far from it. Using folders and subfolders, you can in fact create any type of typical web pages:
 
####  Single Content Page
A standard page is a single content page that shows the data of one text file, like an about page, a contact page, an error page or a single blog post.

####  List Type Page
List type pages list several standard pages on one page, for example, a list of blog articles, a collection of projects or related listing. This is usually done by creating child pages (subfolders) of a parent page and then collect the children on the parent page. But a list type page could also be a category page that collects all pages from all folders that belong to a category.

#### Modular Page
A modular page is similar to a listings page but typically combines content from multiple pages or subpages. A typical modular page is a start page that shows maybe a featured product, a limited number of projects, and the latest posts from the blog or a complex one-pager website.