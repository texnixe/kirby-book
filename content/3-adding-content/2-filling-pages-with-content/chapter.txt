Title: Filling Pages with Content

----

Text: 

### Filling the page with content

Content is added by creating a text file with the extension .txt inside your page folder. You can use any text editor to edit these files. If you create content using the panel, content is edited in your favorite browser.

(infobox: The .txt extension is the default for text files. If you prefer to use the .md extension, you can tell Kirby in your `/site/config/config.php`: <a href="">See Customizing Kirby</a> class: info)

### Anatomy of a content text file

A content text file is made up of fields that contain the actual data. The minimum requirement is a title field. But you can add any field you like and, theoretically, as many fields as you like.

```text
Title: This is a dummy title
 ----
Text: And this is some dummy text
```

To separate one field from the next, you  type  4 dashes (----).

Let's look at some example page types and the sort of data you would probably store in them.

##### A simple blog post
```text
Title:
 ----
Subtitle: 
 ----
Author:
 ----
Date:
 ----
Category:
 ----
Tags:
 ----
Cover Image:
 ----
Text:
```

##### A project page
```text
Title:
 ----
Year:
 ----
Category:
 ----
Images:
 ----
Text:
```