Title: Panel Widgets

----

Text: 

Since version 2.1 of Kirby, you can now extend the panel with widgets. This opens up completely new possibilites of what your editors can do via the panel.

New widgets are stored in `/site/widgets`.

Here is the code of a barebones widget:

```php
<?php
return array(
  'title' => 'Widget Title',
  'html'  => function() {
    // any data for the template
    $data = array();
    return tpl::load(__DIR__ . DS . 'template.php', $data);
  }
);
```